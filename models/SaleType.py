# -*- coding: utf-8 -*-
from odoo import models, fields, api


class SaleType(models.Model):
    _name = 'sale.type'

    # Dominio para productos con existencias
    @api.model
    def check_quantity(self):
        ids = self.env['product.product'].search([('type', '!=', 'service')])
        domain_ids = []
        for p in ids:
            if(p.qty_available > 0):
                domain_ids.append(p.id)

        return [('id', 'in', domain_ids)]

    # definición de campos
    type = fields.Selection([
        ('prepago', 'Prepago'),
        ('plan', 'Plan'),
        ('activacion', 'Activación'),
    ], string='Tipo')
    sale_id = fields.Many2one('sale.order', 'Venta')
    product_id = fields.Many2one(
        'product.product', 'Producto', domain=check_quantity,
    )
    service_id = fields.Many2one(
        'product.product', 'Servicio', domain="[('type', '=', 'service')]"
    )
    serie_id = fields.Many2one('stock.production.lot', 'Serie')
    monthly_rent = fields.Float(
        string='Renta Mensual',
        digits=(10, 2),
        required=True,
    )
    protection = fields.Selection([
        ('0', 'Ninguno'),
        ('55', 'Protección 55'),
        ('105', 'Protección 105'),
        ('155', 'Protección 155'),
    ], string='Protección de equipo')
