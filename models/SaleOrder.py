# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    # definición de campos
    sale_type_ids = fields.Many2many(
        'sale.type',
        string='Tipos de venta',
    )

    # función para eliminar las lineas creadas a partir de sale type
    def delete_sale_order_lines(self):
        for line in self.order_line:
            if(line.is_from_sale_type):
                line.unlink()

    # función para crear lineas de la SO a partir de sale type
    def create_sale_order_lines(self, sale_type_ids, order_id):
        sale_type = self.env['sale.type']
        sale_order_line = self.env['sale.order.line']
        lines = sale_type.browse(sale_type_ids)
        for l in lines:
            if(l.product_id):
                new_line = sale_order_line.create({
                    'product_id': l.product_id.id,
                    'name': l.product_id.name,
                    'order_id': order_id,
                    'product_uom' : l.product_id.uom_id.id,
                    'is_from_sale_type': True
                })
            if(l.service_id):
                new_line = sale_order_line.create({
                    'product_id': l.service_id.id,
                    'name': l.service_id.name,
                    'order_id': order_id,
                    'product_uom' : l.service_id.uom_id.id,
                    'is_from_sale_type': True
                })

    # añadir lineas de tipo de venta a la SO
    @api.model
    def create(self, vals):

        res = super(SaleOrder, self).create(vals)
        if 'sale_type_ids' in vals:
            sale_type_ids = vals['sale_type_ids'][0][2]
            self.create_sale_order_lines(sale_type_ids, res.id)

        return res

    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        if 'sale_type_ids' in vals:
            self.delete_sale_order_lines()
            sale_type_ids = vals['sale_type_ids'][0][2]
            self.create_sale_order_lines(sale_type_ids, self.id)

        return res
