# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

class SaleOrder(models.Model):
    _inherit = 'sale.order.line'

    # definición de campos
    is_from_sale_type = fields.Boolean(
        string='Es de tipo de venta',
        default=False
    )
