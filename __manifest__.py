{
    'name': "IKOM Sales",
    'description': """
        Módulo para gestionar distintos tipos de ventas
    """,
    'author': "Oscar Ramirez",
    'category': 'Sales',
    'version': '0.1',
    'depends': ['base', 'sale', 'product'],
    'data': [
        'views/sale_order_view.xml',
        'views/sale_type_view.xml',
        'ir.model.access.csv',
    ],
}
